#!/usr/bin/python
from __future__ import print_function
import sys, string, readline

memory = [[None, None, 0]] #Subarrays are [index of prev byte in memory, index of next byte in memory, value at pointer]
pointer = 0

def bf_apply(cmd):
	global pointer
	if cmd == '>':
		if memory[pointer][1] != None:
			pointer = memory[pointer][1]
		else:
			memory.append([pointer, None, 0])
			memory[pointer][1] = len(memory) - 1
			pointer = len(memory) - 1
	elif cmd == '<':
		if memory[pointer][0] != None:
			pointer = memory[pointer][0]
		else:
			memory.append([None, pointer, 0])
			memory[pointer][0] = len(memory) - 1
			pointer = len(memory) - 1
	elif cmd == '+':
		memory[pointer][2] = (memory[pointer][2] + 1) % 256
	elif cmd == '-':
		memory[pointer][2] = (memory[pointer][2] - 1) % 256
	elif cmd == ',':
		memory[pointer][2] = ord(raw_input()[0])
	elif cmd == '.':
		try:
			print(chr(memory[pointer][2]), end = '')
		except ValueError:
			print("Error: value " + str(memory[pointer][2]) + " cannot be convered into char")
	return

def closingBracketIndex(cmd, i):
	stack = []
	for index in range(len(cmd)):
		c = cmd[index]
		if c == '[':
			stack.append(index)
		elif c == ']':
			if stack.pop() == i:
				return index
	return -1

def bf_eval(cmd):
	for i in range(len(cmd)):
		if cmd[i] == '[':
			j = closingBracketIndex(cmd, i)
			if j == -1:
				print("Error: missing closing ']'")
			while memory[pointer][2] != 0:
				bf_eval(cmd[i+1:j])
			bf_eval(cmd[j+1:])
			break
		else:
			bf_apply(cmd[i])

def strip(line):
	line_stripped = ''
	for c in line:
		if c in "><+-[],.":
			line_stripped += c
	return line_stripped

def interactive():
	print("Interactive Brainfuck.\nCreated by Jonathan Ting.\n")
	print("Commands:\n> -- increment pointer\n< -- decrement pointer\n+ -- increment value at pointer\n- -- decrement value at pointer")
	print("[ -- begin loop\n] -- end loop\n, -- read one character from input into value at pointer\n. -- print value at pointer to console\n")
	while True:
		try:
			read = raw_input(">>> ")
			bf_eval(strip(read))
		except (EOFError, KeyboardInterrupt) as e:
			print
			return

def interpret(bfkFile):
	allLines = ''
	for line in bfkFile:
		allLines += line
	bf_eval(strip(allLines))

def main(args):
	if len(args) == 0:
		interactive()
	elif len(args) != 1:
		print("Usage:\nbfk -- loads interactive session")
		print("bfk <filename> -- interprets code from file")
	else:
		try:
			f = open(args[0], "r")
			interpret(f)
			f.close()
		except IOError:
			print('Error: file does not exist')
			return

if __name__ == "__main__":
	main(sys.argv[1:])