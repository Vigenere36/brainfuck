# Brainfuck Interpreter #

Usage:

bfk -- loads interactive session

bfk <filename> -- interprets code in file

If above doesn't work, run python bfk.py [<filename>]